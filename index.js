var request = require('request'),
    querystring = require('querystring');


var uo_api = (function () {

    var settings = {
        apiKey : '',
        baseUrl : 'http://uoweb1.ncl.ac.uk/api/v1'
    };

    var apiResult = function(data) {
        this.data = data;

        this.toTimeSeries = function(){
            return {};
        }
    };

    var queryString = '';

    var apiKey = function (key) {
        if (key) {
            settings.apiKey = key;
            return uo_api;
        } else {
            if (settings.apiKey !== ''){
                return settings.apiKey;
            } else {
                throw new EvalError('API Key Not set');
            }

        }
    };

    var sensors = function() {
        queryString = settings.baseUrl + '/sensors';

        this.data = function() {

            if (queryString !== settings.baseUrl + '/sensors') {
                throw new SyntaxError('Syntax Error');
            }

            queryString += '/data';
            return uo_api;
        };

        this.raw = function() {

            if (queryString !== settings.baseUrl + '/sensors/data') {
                throw new SyntaxError('Syntax Error');
            }

            queryString += '/raw';
            return queryString;
        };
    };









    return {
        apiKey: apiKey,
        sensors : sensors
    };

})();

module.exports = uo_api;
